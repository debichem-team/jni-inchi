Source: jni-inchi
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders:
 Andrius Merkys <merkys@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 junit4 (>= 4.12),
 libexec-maven-plugin-java,
 libinchi-dev (>= 1.07.1+dfsg-2),
 liblog4j1.2-java,
 maven-debian-helper (>= 2.1),
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debichem-team/jni-inchi.git
Vcs-Browser: https://salsa.debian.org/debichem-team/jni-inchi
Homepage: https://jni-inchi.sourceforge.net

Package: libjni-inchi-java
Section: java
Architecture: all
Depends:
 libjni-inchi-jni (>= ${binary:Version}),
 ${maven:CompileDepends},
 ${maven:Depends},
 ${misc:Depends},
Suggests:
 ${maven:OptionalDepends},
Description: Java Native Interface wrapper for InChI
 JNI-InChI provides JNI (Java Native Interface) wrappers for the InChI
 (International Chemical Identifier) C library distributed by IUPAC.
 All of the features from the InChI libarary are supported:
 .
  * Standard and Non-Standard InChI generation from structures with 3D, 2D,
    or no coordinates
  * Structure generation (without coordinates) from InChI
  * InChIKey generation
  * Check InChI / InChIKey
  * InChI-to-InChI conversion
  * AuxInfo to InChI input
  * Access to the full range of options supported by InChI
  * Full support for InChI's handling of stereochemistry

Package: libjni-inchi-jni
Section: java
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Java Native Interface wrapper for InChI (shared library)
 JNI-InChI provides JNI (Java Native Interface) wrappers for the InChI
 (International Chemical Identifier) C library distributed by IUPAC.
 All of the features from the InChI libarary are supported:
 .
  * Standard and Non-Standard InChI generation from structures with 3D, 2D,
    or no coordinates
  * Structure generation (without coordinates) from InChI
  * InChIKey generation
  * Check InChI / InChIKey
  * InChI-to-InChI conversion
  * AuxInfo to InChI input
  * Access to the full range of options supported by InChI
  * Full support for InChI's handling of stereochemistry
 .
 This package contains the shared library.

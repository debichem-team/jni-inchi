jni-inchi (0.8+dfsg-8) unstable; urgency=medium

  * Adapt to inchi v1.07.1 (Closes: #1080517)
  * Mark the patches not needing forwarding.
  * Reduce boilerplate.

 -- Andrius Merkys <merkys@debian.org>  Fri, 06 Sep 2024 02:14:40 -0400

jni-inchi (0.8+dfsg-7) unstable; urgency=medium

  * Add explicit dependency on liblog4j1.2-java (Closes: #1028803)
  * Bump copyright years.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.6.2, no changes needed.

 -- Andrius Merkys <merkys@debian.org>  Mon, 16 Jan 2023 05:11:20 -0500

jni-inchi (0.8+dfsg-6) unstable; urgency=medium

  * Rewriting debian/watch with mode=svn.

 -- Andrius Merkys <merkys@debian.org>  Fri, 31 Jul 2020 06:22:57 -0400

jni-inchi (0.8+dfsg-5) unstable; urgency=medium

  * Splitting override_dh_auto_install to -indep and -arch.

 -- Andrius Merkys <merkys@debian.org>  Wed, 13 Nov 2019 03:53:50 -0500

jni-inchi (0.8+dfsg-4) unstable; urgency=medium

  * Arch-dependent build has the same build-depends as arch-independent build.

 -- Andrius Merkys <merkys@debian.org>  Tue, 12 Nov 2019 12:18:42 -0500

jni-inchi (0.8+dfsg-3) unstable; urgency=medium

  * Adding symlink libjni-inchi-jni.poms -> libjni-inchi-java.poms to fix
    build-arch builds.

 -- Andrius Merkys <merkys@debian.org>  Tue, 12 Nov 2019 10:58:52 -0500

jni-inchi (0.8+dfsg-2) unstable; urgency=medium

  * Fixing debian/copyright (package is released under LGPL-3+).

 -- Andrius Merkys <merkys@debian.org>  Tue, 12 Nov 2019 01:45:44 -0500

jni-inchi (0.8+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #939842).

 -- Andrius Merkys <merkys@debian.org>  Mon, 02 Sep 2019 01:22:12 -0400
